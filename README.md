# F800GS LED Turn Signals

LED retrofit for 2008 large turn Signals  
Fits directly in place of lamp and reflector  
Option for single row LEDs for lower consumption or double row for extra light intensity  
Independent Dual LED arrays for redundancy  
Efficient switching current control

* Output: 231/300 lumens  
* Power Consumption: 1.8/2.2W  
* Input voltage: 9-24V DC

![Front Side](https://gitlab.com/drid/f800gs-led-turn-signal/-/raw/master/front.jpg)
![Back Side](https://gitlab.com/drid/f800gs-led-turn-signal/-/raw/master/back.jpg)

A holder pin needs to be 3D printed in order to secure the housing to the frame   
FreeCAD and STL file in mechanical folder

## Assembly
PCB supports two configurations, single and dual row, see schematic before ordering parts  
In single row configuration LEDs need to be driven at a higher current to avoid triggering LAMP error that occurs when current is below 131mA  

## Coding
ZFE needs to be coded for LED turn signals to avoid LAMP error

## Licence
All files released under [CERN-OHL-S](https://ohwr.org/cern_ohl_s_v2.txt) except LUXEON_2835_20160407_STEP.STEP

## Attribution
LED STEP models from LUMILEDS https://www.lumileds.com/products/color-leds/luxeon-2835-color-line/  
Colored with FreeCAD
